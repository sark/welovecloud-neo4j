Neo4j 1.9
=========

** From Neo4j offical repository **

** Packages **

- openjdk-7
- neo4j

Uses persistent data directory (see command below). 

Use `sudo docker run -d -p 7474:7474 -v /var/lib/neo4j/data:/var/lib/neo4j/data welovecloud/neo4j` to run.

Use `sudo docker logs container-id` to get output.

To access web-gui navigate to http://127.0.0.1:7474 . 